/**
 * @file
 * blocktabs behaviors.
 */
(function ($, Drupal) {

  'use strict';

  /**
   * Add jquery ui tabs effect.
   *
   * @type {Drupal~behavior}
   *
   * @prop {Drupal~behaviorAttach} attach
   *
   */
  Drupal.behaviors.blocktabs_jquery_ui_vertical = {
    attach: function (context, settings) {
      $(context).find('div.blocktabs.vertical').each(function () {
        $(this).tabs({
          event: 'click'
        });
        $(this).addClass('ui-tabs-vertical ui-helper-clearfix');
        $(this).find('li').removeClass('ui-corner-top').addClass('ui-corner-left');
      });
    }
  };

}(jQuery, Drupal));

