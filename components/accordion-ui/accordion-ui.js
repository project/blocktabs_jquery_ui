/**
 * @file
 * blocktabs behaviors.
 */
(function ($, Drupal) {

  'use strict';

  /**
   * Add jquery ui accordion effect.
   *
   * @type {Drupal~behavior}
   *
   * @prop {Drupal~behaviorAttach} attach
   *
   */
  Drupal.behaviors.blocktabs_jquery_ui_accordion = {
    attach: function (context, settings) {
      $(context).find('div.blocktabs.accordion').each(function () {
        $(this).accordion({
          collapsible: true,
          event: 'click'
        });

      });
    }
  };

}(jQuery, Drupal));



