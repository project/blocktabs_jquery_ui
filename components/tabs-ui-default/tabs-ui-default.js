/**
 * @file
 * blocktabs behaviors.
 */
(function ($, Drupal) {

  'use strict';

  /**
   * Add jquery ui tabs effect.
   *
   * @type {Drupal~behavior}
   *
   * @prop {Drupal~behaviorAttach} attach
   *
   */
  Drupal.behaviors.blocktabs_jquery_ui_default = {
    attach: function (context, settings) {
      $(context).find('div.blocktabs.default').each(function () {
        jQuery(this).tabs({
          event: 'click'
        });
      });
    }
  };

}(jQuery, Drupal));

